


 var activeImage=  0;
function initShowRoom(cars){
       
        for (var i = 0 ; i< cars.length; i++){
            car =cars[i];
            var className= (i==0)? "" : "hidden" ;
            var id= "img"+i;
            
            var img = $("<img>",{ class:"car-img frame "+className, src:car.image , id:id });
            var desc = $("<div>",{class:"car-desc frame "+className});
            desc.html(car.name);
            
          
           
            $(".showroom").append(img);
            $(".showroom").append(desc);
        }

        $(".arrow-right").click(function(){
            $("#img"+activeImage).css("left",0);
            $("#img"+activeImage).animate({"left":"100%"},function(){
                activeImage++;
                
                
                activeImage= (activeImage>=cars.length) ? 0 : activeImage ;
                
                nextImg = $("#img"+activeImage);
                nextImg.removeClass("hidden");
                console.log(nextImg);
                nextImg.css({"right":"100%"});
                nextImg.animate({"right": "0" },function(){
                    $(this).removeClass("left");
                    console.log(cars[activeImage].name);
                    $(".car-desc").html(cars[activeImage].name);
                });

                $(this).addClass("hidden");
               
                $(this).css("left",0);
                $(this).css("right",0);
            });
           
           
        })
        $(".arrow-left").click(function(){
            $("#img"+activeImage).css("right","0");
            $("#img"+activeImage).animate({"right":"100%"},function(){
                activeImage++;
                
                
                activeImage= (activeImage>=cars.length) ? 0 : activeImage ;
                
                nextImg = $("#img"+activeImage);
                nextImg.removeClass("hidden");
                nextImg.css("left","100%");
                nextImg.animate({"left": "0" },function(){
                    $(this).removeClass("left");
                    $(".car-desc").html(cars[activeImage].name);
                });

                $(this).addClass("hidden");
               
                $(this).css("left",0);
                $(this).css("right",0);
            });
        })


        $(window).resize(function(){
            var windowWidth = ($(window).width());
            var offset= $(window).width()/10;

            $(".car-img").each(function(){
                var ratio = $(this).width()/$(this).height();
                $(this).css("max-width", windowWidth-offset);
                $(this).css("max-height", (windowWidth-offset) * ratio);
                $(".slide-show").css("max-height", $(this).css("max-width"));
                
            });

        });



}