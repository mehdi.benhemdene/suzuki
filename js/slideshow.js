var activeImage = 0;
var imagesCount = 1;
var swipeEvent;
var swipeFactor;
var loaded=  0;
var defaultAdj= 25;
function initSlideShow(images, colors,roofColors) {

            imagesCount= images.length;
            for (var i = 0; i < imagesCount; i++) {
                className = (i == 0) ? "frame" : "hidden frame";
                id = "img" + i;

               
                var img = $("<img>", {
                    "src": images[i].url,
                    class: className,
                    id: ("img" + i)
                });

                $(".slide-show").append(img);
            }
            percentage = (1/colors.length)*100 - 5;
            
            for (var i = 0; i < colors.length; i++) {
                
                widget = $("<div>", {
                    class: "color-widget body-widget",
                    "color": colors[i]
                });
               

                $(".widgets-right").append(widget);
                
            }
            
            for (var i = 0; i < roofColors.length; i++) {
                
                widget = $("<div>", {
                    class: "color-widget roof-widget",
                    "color": roofColors[i]
                });
               

                $(".body-widgets").append(widget);
                
            }

            
            $(".frame").load(function () {
                ratio = $((this)).height() / $(this).width();
                offset= $(window).width()/10;
                $(this).css("max-width", $(window).width()-offset);
                $(this).css("max-height", ($(window).width()-offset) * ratio);
                console.log( ($(window).width()-offset) * ratio);
                $(".slide-show").css("max-height", $(this).height() + 100);
                loaded++;
                if (loaded==1){
                   
                }
                if (loaded==imagesCount){
                    $(".loading").fadeOut(2000);
                    $(".color-filter").css("height",$("#img0").height());
                    $(".color-filter").css("width",$("#img0").width());
                    var adjustment= images[0].adjustment;
                    if (images[0].adjustment !== undefined){
                        $(".roof").css("height",adjustment+"%");
                        $(".body").css("height",(100-adjustment)+"%");
                    }
                    else{
                        $(".roof").css("height",defaultAdj+"%");
                        $(".body").css("height",(100-defaultAdj)+"%");
                    }
                }
            })
            
            
            $(window).resize(function () {
                $(".color-filter").css("height",$("#img"+activeImage).height());
                $(".color-filter").css("width",$("#img"+activeImage).width());
                $(".frame").each(function () {
                    ratio = $((this)).height() / $(this).width();
                    offset= $(window).width()/10;
                    $(this).css("max-width", $(window).width()-offset);
                    $(this).css("max-height", ($(window).width()-offset) * ratio);
                    $(".slide-show").css("max-height", $(".frame").height() + 100);
                    
                })

            })


            $('.slide-show img').on('dragstart', function (event) {
                event.preventDefault();
            });


            $(".slide-show").on("swipeleft", swipeleftHandler);
            $(".slide-show").on("swiperight", swipeRightHandler);
            swipeFactor = 30;
            function swipeleftHandler(event) {
                swipeEvent=event;
                var duration = event.swipestop.time - event.swipestart.time;
                console.log(duration);
                nextImage = (activeImage - 1);
                nextImage = (nextImage == -1) ? (imagesCount-1) : nextImage;
                $(".slide-show .frame").addClass("hidden");
                $("#img" + nextImage).removeClass("hidden");
                activeImage = nextImage;
                $(".color-filter").css("height",$("#img"+activeImage).height());
                $(".color-filter").css("width",$("#img"+activeImage).width());
                var adjustment= images[activeImage].adjustment;
                if (images[activeImage].adjustment !== undefined){
                    $(".roof").css("height",adjustment+"%");
                    $(".body").css("height",(100-adjustment)+"%");
                }
                else{
                    $(".roof").css("height",defaultAdj+"%");
                    $(".body").css("height",(100-defaultAdj)+"%");
                }
            }

            function swipeRightHandler(event) {
                nextImage = (activeImage + 1);
                nextImage = (nextImage >= imagesCount) ? 0 : nextImage;
                $(".slide-show .frame").addClass("hidden");
                $("#img" + nextImage).removeClass("hidden");
                activeImage = nextImage;
                $(".color-filter").css("height",$("#img"+activeImage).height());
                $(".color-filter").css("width",$("#img"+activeImage).width());
                var adjustment= images[activeImage].adjustment;
                if (images[activeImage].adjustment !== undefined){
                
                    $(".roof").css("height",adjustment+"%");
                    $(".body").css("height",(100-adjustment)+"%");
                }
                else{
                    $(".roof").css("height",defaultAdj+"%");
                    $(".body").css("height",(100-defaultAdj)+"%");
                }
            }



            $(".color-widget").each(function () {
                $(this).css("background-color", $(this).attr("color"));
            });

            $(".body-widget").click(function () {
                $(".body").css("background-color", $(this).attr("color"));
                
            })

            $(".roof-widget").click(function () {
                $(".roof").css("background-color", $(this).attr("color"));
                
            })


            
            $(".ui-loader").hide();
        }